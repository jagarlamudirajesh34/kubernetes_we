<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
div.scroll-container {
  background-color: #333;
  overflow: auto;
  white-space: nowrap;
  padding: 10px;
}

div.scroll-container img {
  padding: 10px;
}
</style>
</head>
<body>
<h1 align="center" style="color:red">Welcome to SRISH Technologies, MMC, KPHB, Hyderabad</h1>
<h1 align="center" style="color:red"> SIT Env - DevOps with AWS & Linux Training</h1>
<h1 align="center" style="color:red">9515132351 || 9515222069 || trainings@srishsoft.com || https://srishsoft.com</h1>
<hr>
<h1 align="center" style="color:green">SRISH's Live Training Interactive Sessions - Offline || Online</h1>
<div class="scroll-container">
  <img src="https://aug02pub.s3.ap-south-1.amazonaws.com/Sagar1.jpeg" alt="Cinque Terre" width="600" height="400">
  <img src="https://aug02pub.s3.ap-south-1.amazonaws.com/Image1.png" alt="Cinque Terre" width="600" height="400">
  <img src="https://aug02pub.s3.ap-south-1.amazonaws.com/Image2.jpg" alt="Forest" width="600" height="400">
  <img src="https://aug02pub.s3.ap-south-1.amazonaws.com/Rajesh1.jpeg" alt="Forest" width="600" height="400">
  <img src="https://aug02pub.s3.ap-south-1.amazonaws.com/Image3.jpg" alt="Northern Lights" width="600" height="400">
  <img src="https://aug02pub.s3.ap-south-1.amazonaws.com/Image4.jpg" alt="Mountains" width="600" height="400">
</div>
<hr>
<h1 align="center" style="color:green">SRISH's Will do bellow Live Projects Implementation as part of Training</h1>
<div align="center">
  <img src="https://aug02pub.s3.ap-south-1.amazonaws.com/Project+-+2.jpg" alt="Cinque Terre" height="500" align="center">
  <hr>
  <img src="https://aug02pub.s3.ap-south-1.amazonaws.com/Project+-+1.jpg" alt="Cinque Terre" height="900" align="center">
  <hr>
  <img src="https://aug02pub.s3.ap-south-1.amazonaws.com/Project3.jpg	" alt="Cinque Terre" height="900" align="center"> 
</div>
<hr>
<h2 align="center" style="color:red">Address: #124, #220, #221, #503, Manjeera Majestic Commercial Complex, Hi-Tech City Road, KPHB Phase-3, Hyderabad.</h2>
<hr>
</body>
</html>
